package com.bswenson.weatherwatcher

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.location.LocationProvider
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.bswenson.weatherwatcher.alerts.AlertsDetailFragment
import com.bswenson.weatherwatcher.alerts.AlertsFragment
import com.bswenson.weatherwatcher.alerts.AlertsViewModel
import com.bswenson.weatherwatcher.model.Alert
import com.bswenson.weatherwatcher.weekly.WeeklyViewModel
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import java.util.*

private const val ARG_ALERT_ID = "alert_id"
private const val PERM_LOCA_ID = 42

class MainActivity : AppCompatActivity(), AlertsFragment.Callbacks {

    private lateinit var viewModel: WeeklyViewModel
    private lateinit var alertsViewModel: AlertsViewModel

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    private lateinit var sharedPreferences: SharedPreferences

    private var requestingLocationUpdates = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(findViewById(R.id.my_toolbar))

        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)

        navView.setupWithNavController(navController)

        val permissionAccessCoarseLocationApproved = ActivityCompat
            .checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED

        if (!permissionAccessCoarseLocationApproved) {
            Log.d("MainActivity", "permission denied, requesting permissions")
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                PERM_LOCA_ID)
        }else{
            Log.d("MainActivity", "permission allowed, requesting = true")
            requestingLocationUpdates = true
        }



        locationCallback = object : LocationCallback() {
            override fun onLocationResult(result: LocationResult?) {
                Log.d("MainActivity", "onLocationResult()")
                result ?: return
                Log.d("MainActivity", "${result.lastLocation}")
                viewModel.latLiveData.value = result.lastLocation.latitude
                viewModel.lonLiveData.value = result.lastLocation.longitude
            }
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        viewModel = ViewModelProviders.of(this).get(WeeklyViewModel::class.java)
        alertsViewModel = ViewModelProviders.of(this).get(AlertsViewModel::class.java)

    }

    override fun onResume() {
        super.onResume()
        Log.d("MainActivity", "onResume()")
        if (requestingLocationUpdates) startLocationUpdates()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }


    private fun startLocationUpdates() {
        Log.d("MainActivity", "startLocationUpdates()")
        val locationRequest = LocationRequest().apply {
            priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
            interval = 10
        }
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            PERM_LOCA_ID -> {
                requestingLocationUpdates = grantResults.first() == PackageManager.PERMISSION_GRANTED
            }
            else -> {}
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.appbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.gps -> {
                Log.d("MainActivity", "manually started with gps button")
                startLocationUpdates()
                requestingLocationUpdates = true
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onLongClick(alert: Alert) {
        val alertDialog: AlertDialog? = this.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setTitle("Delete '${alert.title}' ?")
                setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, id ->
                    alertsViewModel.deleteAlert(alert)
                })

                setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->

                })
            }
            builder.create()
        }
        alertDialog?.show()
    }

    override fun onClick(alert: Alert) {
        findNavController(R.id.nav_host_fragment).navigate(
            R.id.action_navigation_alerts_to_alertsDetailFragment,
            Bundle().apply { putSerializable(ARG_ALERT_ID, alert.id) })
    }
}
