package com.bswenson.weatherwatcher.weekly

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.bswenson.weatherwatcher.data.OpenWeatherRepository
import com.bswenson.weatherwatcher.model.HourlyWeatherResponse
import com.bswenson.weatherwatcher.model.OpenWeatherResponse

class WeeklyViewModel : ViewModel() {


    val latLiveData = MutableLiveData<Double>(0.0)
    val lonLiveData = MutableLiveData<Double>(0.0)
    val dayIndex = MutableLiveData<Int>(0)

    val openWeatherResponse: LiveData<OpenWeatherResponse> = Transformations.switchMap(latLiveData){
        lat -> OpenWeatherRepository().oneCall(latLiveData.value!!, lonLiveData.value!!)
    }
}
