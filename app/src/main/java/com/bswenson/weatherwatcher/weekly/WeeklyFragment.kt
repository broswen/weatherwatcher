package com.bswenson.weatherwatcher.weekly

import android.content.Context
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.icu.text.SimpleDateFormat
import android.location.Location
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bswenson.weatherwatcher.R
import com.bswenson.weatherwatcher.model.DailyWeatherResponse
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import java.util.*


class WeeklyFragment : Fragment() {

    companion object {
        fun newInstance() = WeeklyFragment()
    }

    private lateinit var viewModel: WeeklyViewModel

    private lateinit var recyclerView: RecyclerView



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.weekly_fragment, container, false)

        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(requireActivity()).get(WeeklyViewModel::class.java)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.openWeatherResponse.observe(viewLifecycleOwner, Observer {
            recyclerView.adapter = WeeklyWeatherAdapter(it.daily.take(7))
        })
    }

    private fun getWeatherIcon(main: String): Int = when(main){
        "Clouds" -> R.drawable.ic_wi_day_sunny_overcast
        "Clear" -> R.drawable.ic_wi_day_sunny
        "Snow" -> R.drawable.ic_wi_snow
        "Rain" -> R.drawable.ic_wi_rain
        "Drizzle" -> R.drawable.ic_wi_sprinkle
        "Thunderstorm" -> R.drawable.ic_wi_thunderstorm
        else -> R.drawable.ic_wi_fog
    }

    private inner class WeeklyItemHolder(val linearLayout: LinearLayout) : RecyclerView.ViewHolder(linearLayout){
        private lateinit var iconImageView: ImageView
        private lateinit var humidityTextView: TextView
        private lateinit var tempTextView: TextView
        private lateinit var dateTextView: TextView

        fun bind(dailyWeatherResponse: DailyWeatherResponse){
            humidityTextView = linearLayout.findViewById(R.id.humidityTextView)
            tempTextView = linearLayout.findViewById(R.id.tempTextView)
            dateTextView = linearLayout.findViewById(R.id.dateTextView)
            iconImageView = linearLayout.findViewById(R.id.iconImageView)

            iconImageView.setImageDrawable(getDrawable(requireContext(), getWeatherIcon(dailyWeatherResponse.weather.first().main)))

            humidityTextView.text = "${dailyWeatherResponse.humidity}% Humidity"
            tempTextView.text = "${dailyWeatherResponse.temp.day} F"
            dateTextView.text = "${SimpleDateFormat("EEE, MMM d").format(Date(dailyWeatherResponse.dt * 1000))}"
        }
    }

    private inner class WeeklyWeatherAdapter(val data: List<DailyWeatherResponse>) : RecyclerView.Adapter<WeeklyItemHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeeklyItemHolder {
            val view = layoutInflater.inflate(R.layout.weekly_list_item, parent, false) as LinearLayout
            return WeeklyItemHolder(view)
        }

        override fun getItemCount() = data.size

        override fun onBindViewHolder(holder: WeeklyItemHolder, position: Int) {
            holder.bind(data[position])
        }

    }

}
