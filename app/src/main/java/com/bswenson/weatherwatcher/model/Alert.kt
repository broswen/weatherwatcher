package com.bswenson.weatherwatcher.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Alert (@PrimaryKey val id: UUID = UUID.randomUUID(),
                  var title: String = "golfing weather",
                  var lowTemp: Double = 0.0,
                  var highTemp: Double = 100.0,
                  var weather: String = "sunny",
                  var lowWind: Double = 0.0,
                  var highWind: Double = 30.0)