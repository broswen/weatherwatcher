package com.bswenson.weatherwatcher.model

import androidx.lifecycle.LiveData
import androidx.room.*
import java.util.*

@Dao
interface AlertDao {
    @Query("SELECT * FROM alert")
    fun getAlerts(): LiveData<List<Alert>>

    @Query("SELECT * FROM alert WHERE id=(:id)")
    fun getAlert(id: UUID) : LiveData<Alert>

    @Update
    fun updateAlert(alert: Alert)

    @Insert
    fun addAlert(alert: Alert)

    @Delete
    fun deleteAlert(alert: Alert)
}