package com.bswenson.weatherwatcher.model

data class OpenWeatherResponse(
    val lat: Double,
    val lon: Double,
    val timezone: String,
    val current: HourlyWeatherResponse,
    val hourly: List<HourlyWeatherResponse>,
    val daily: List<DailyWeatherResponse>
)

data class HourlyWeatherResponse(
    val dt: Long,
    val temp: Double,
    val feels_like: Double,
    val pressure: Int,
    val humidity: Int,
    val dew_point: Double,
    val clouds: Int,
    val wind_speed: Double,
    val wind_deg: Int,
    val weather: List<WeatherDescription>
)

data class DailyWeatherResponse(
    val dt: Long,
    val sunrise: Int,
    val sunset: Int,
    val temp: Temp,
    val feels_like: FeelsLike,
    val pressure: Int,
    val humidity: Int,
    val dew_point: Double,
    val uvi: Double,
    val clouds: Int,
    val wind_speed: Double,
    val wind_deg: Int,
    val wind_gust: Double,
    val weather: List<WeatherDescription>
)

data class Temp(
    val day: Double,
    val min: Double,
    val max: Double,
    val night: Double,
    val eve: Double,
    val morn: Double
)

data class FeelsLike(
    val day: Double,
    val night: Double,
    val eve: Double,
    val morn: Double
)

data class WeatherDescription(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
)