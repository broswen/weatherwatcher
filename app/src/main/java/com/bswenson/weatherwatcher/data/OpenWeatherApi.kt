package com.bswenson.weatherwatcher.data

import com.bswenson.weatherwatcher.model.OpenWeatherResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

private const val api_key = "5be6e17f0b622b5741fb32e970a877f1"

interface OpenWeatherApi {
    //@GET("data/2.5/onecall?lat=46.73&lon=-94.69&appid=$api_key&units=imperial")
    @GET("data/2.5/onecall?appid=$api_key&units=imperial")
    fun oneCall(@Query("lat") lat: Double, @Query("lon") long: Double): Call<OpenWeatherResponse>
}