package com.bswenson.weatherwatcher.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bswenson.weatherwatcher.model.OpenWeatherResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class OpenWeatherRepository {

    private val openWeatherApi: OpenWeatherApi

    init {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://api.openweathermap.org/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        openWeatherApi = retrofit.create(OpenWeatherApi::class.java)
    }

    fun oneCall(lat: Double, lon: Double): LiveData<OpenWeatherResponse>{
        Log.d("OpenWeatherRepository", "fetching oneCall response")

        val responseLiveData: MutableLiveData<OpenWeatherResponse> = MutableLiveData()
        val oneCallRequest: Call<OpenWeatherResponse> = openWeatherApi.oneCall(lat, lon)

        oneCallRequest.enqueue(object: Callback<OpenWeatherResponse>{
            override fun onFailure(call: Call<OpenWeatherResponse>, t: Throwable) {
                Log.e("OpenWeatherRepository", "Error fetching oneCall: $t")
            }

            override fun onResponse(call: Call<OpenWeatherResponse>, response: Response<OpenWeatherResponse>) {
                val openWeatherResponse: OpenWeatherResponse? = response.body()

                responseLiveData.value = openWeatherResponse
            }
        })

        return responseLiveData
    }
}