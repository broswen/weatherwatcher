package com.bswenson.weatherwatcher.data

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import com.bswenson.weatherwatcher.model.Alert
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.IllegalStateException
import java.util.*
import java.util.concurrent.Executors

class AlertsRepository private constructor(context: Context) {
    private val database: AlertsDatabase = Room.databaseBuilder(
        context.applicationContext,
        AlertsDatabase::class.java,
        "alerts-database")
        .build()

    private val alertDao = database.alertDao()
    private val executor = Executors.newSingleThreadExecutor()
    private val filesDir = context.applicationContext.filesDir

    fun getAlerts() : LiveData<List<Alert>> = alertDao.getAlerts()

    fun getAlert(uuid: UUID) : LiveData<Alert> = alertDao.getAlert(uuid)

    fun updateAlert(alert: Alert){
        GlobalScope.launch{
            alertDao.updateAlert(alert)
        }
        //executor.execute{
        //    alertDao.updateAlert(alert)
        //}
    }

    fun addAlert(alert: Alert){
        GlobalScope.launch {
            alertDao.addAlert(alert)
        }
        //executor.execute{
        //    alertDao.addAlert(alert)
        //}
    }

    fun deleteAlert(alert: Alert){
        GlobalScope.launch {
            alertDao.deleteAlert(alert)
        }
    }

    companion object{
        private var INSTANCE : AlertsRepository? = null

        fun initialize(context: Context){
            if(INSTANCE == null){
                INSTANCE = AlertsRepository(context)
            }
        }

        fun get(): AlertsRepository{
            return INSTANCE ?: throw IllegalStateException("alerts repository must be initialized")
        }
    }
}