package com.bswenson.weatherwatcher.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.bswenson.weatherwatcher.model.Alert
import com.bswenson.weatherwatcher.model.AlertDao

@Database(entities = [Alert::class], version=1, exportSchema = false)
@TypeConverters(AlertTypeConverters::class)
abstract class AlertsDatabase : RoomDatabase(){
    abstract fun alertDao(): AlertDao
}