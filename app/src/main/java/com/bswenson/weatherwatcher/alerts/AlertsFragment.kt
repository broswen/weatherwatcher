package com.bswenson.weatherwatcher.alerts

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.bswenson.weatherwatcher.R
import com.bswenson.weatherwatcher.alerts.AlertsFragment.Callbacks
import com.bswenson.weatherwatcher.model.Alert
import com.google.android.material.floatingactionbutton.FloatingActionButton


private const val ARG_ALERT_ID = "alert_id"

class AlertsFragment : Fragment(){

    companion object {
        fun newInstance() = AlertsFragment()
    }

    interface Callbacks{
        fun onLongClick(alert: Alert)
        fun onClick(alert: Alert)
    }

    private var callbacks: Callbacks? = null

    private lateinit var viewModel: AlertsViewModel

    private lateinit var recyclerView: RecyclerView
    private lateinit var fab: FloatingActionButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.alerts_fragment, container, false)

        fab = view.findViewById(R.id.floatingActionButton)
        fab.setOnClickListener {
            val alert = Alert()
            viewModel.addAlert(alert)
            findNavController().navigate(
                R.id.action_navigation_alerts_to_alertsDetailFragment,
                Bundle().apply { putSerializable(ARG_ALERT_ID, alert.id)}
            )
        }

        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callbacks = context as Callbacks
    }

    override fun onDetach() {
        super.onDetach()
        callbacks = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(requireActivity()).get(AlertsViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.alertsListLiveData.observe(viewLifecycleOwner) {
            recyclerView.adapter = AlertAdapter(it)
        }
    }

    private inner class AlertItemHolder(val linearLayout: LinearLayout) : RecyclerView.ViewHolder(linearLayout){
        private lateinit var weatherTextView: TextView
        private lateinit var windTextView: TextView
        private lateinit var tempTextView: TextView
        private lateinit var titleTextView: TextView

        fun bind(alert: Alert){
            weatherTextView = linearLayout.findViewById(R.id.weatherTextView)
            windTextView = linearLayout.findViewById(R.id.windRangeTextView)
            tempTextView = linearLayout.findViewById(R.id.tempRangeTextView)
            titleTextView = linearLayout.findViewById(R.id.titleTextView)
            titleTextView.text = alert.title
            weatherTextView.text = alert.weather.capitalize()
            windTextView.text = "${alert.lowWind.toInt()}-${alert.highWind.toInt()} MPH"
            tempTextView.text = "${alert.lowTemp.toInt()}-${alert.highTemp.toInt()} F"

            linearLayout.setOnLongClickListener {
                callbacks?.onLongClick(alert)
                true
            }

            linearLayout.setOnClickListener {
                callbacks?.onClick(alert)
            }
        }
    }

    private inner class AlertAdapter(val data: List<Alert>) : RecyclerView.Adapter<AlertItemHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlertItemHolder {
            val view = layoutInflater.inflate(R.layout.alerts_list_item, parent, false) as LinearLayout
            return AlertItemHolder(view)
        }

        override fun getItemCount() = data.size

        override fun onBindViewHolder(holder: AlertItemHolder, position: Int) {
            holder.bind(data[position])
        }

    }
}

