package com.bswenson.weatherwatcher.alerts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.bswenson.weatherwatcher.data.AlertsRepository
import com.bswenson.weatherwatcher.model.Alert
import java.util.*

class AlertsDetailViewModel : ViewModel() {
    private val alertsRepository = AlertsRepository.get()
    private val alertIdLiveData = MutableLiveData<UUID>()

    var alertLiveData: LiveData<Alert?> =
        Transformations.switchMap(alertIdLiveData){
            id -> alertsRepository.getAlert(id)
        }

    fun loadAlert(id: UUID){
        alertIdLiveData.value = id
    }

    fun updateAlert(alert: Alert){
        alertsRepository.updateAlert(alert)
    }
}
