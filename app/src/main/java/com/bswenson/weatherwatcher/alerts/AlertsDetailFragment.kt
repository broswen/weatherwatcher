package com.bswenson.weatherwatcher.alerts

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.SeekBar
import android.widget.Spinner
import androidx.lifecycle.Observer

import com.bswenson.weatherwatcher.R
import com.bswenson.weatherwatcher.model.Alert
import java.util.*

private const val ARG_ALERT_ID = "alert_id"

class AlertsDetailFragment : Fragment() {

    companion object {
        fun newInstance(id: UUID): AlertsDetailFragment {
            val args = Bundle().apply {
                putSerializable(ARG_ALERT_ID, id)
            }

            return AlertsDetailFragment().apply {
                arguments = args
            }
        }
    }

    private lateinit var viewModel: AlertsDetailViewModel

    private lateinit var alert: Alert

    private lateinit var titleEditText: EditText
    private lateinit var weatherSpinner: Spinner

    private lateinit var minTempSeekbar: SeekBar
    private lateinit var maxTempSeekbar: SeekBar
    private lateinit var minWindSeekbar: SeekBar
    private lateinit var maxWindSeekbar: SeekBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.alerts_detail_fragment, container, false)

        titleEditText = view.findViewById(R.id.titleEditText)
        weatherSpinner = view.findViewById(R.id.weatherSpinner)

        minTempSeekbar = view.findViewById(R.id.minTempSeekbar)
        maxTempSeekbar = view.findViewById(R.id.maxTempSeekbar)
        minWindSeekbar = view.findViewById(R.id.minWindSeekbar)
        maxWindSeekbar = view.findViewById(R.id.maxWindSeekbar)


        titleEditText.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                alert.title = s.toString()
            }

        })

        maxWindSeekbar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                alert.highWind = progress.toDouble()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })

        minWindSeekbar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                alert.lowWind = progress.toDouble()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })

        maxTempSeekbar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                alert.highTemp= progress.toDouble()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })

        minTempSeekbar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                alert.lowTemp= progress.toDouble()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })


        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(requireActivity()).get(AlertsDetailViewModel::class.java)
        alert = Alert()
        val alertId: UUID = arguments?.getSerializable(ARG_ALERT_ID) as UUID
        viewModel.loadAlert(alertId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.alertLiveData.observe(viewLifecycleOwner, Observer{
            it?.let {
                this.alert = it
                updateUI()
            }
        })

    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
        viewModel.updateAlert(alert)
    }

    private fun updateUI(){
        Log.d("AlertsDetailFragment", "updateUI()")
        titleEditText.setText(alert.title)
        minTempSeekbar.progress = alert.lowTemp.toInt()
        maxTempSeekbar.progress = alert.highTemp.toInt()
        minWindSeekbar.progress = alert.lowWind.toInt()
        maxWindSeekbar.progress = alert.highWind.toInt()
    }

}
