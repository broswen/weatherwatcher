package com.bswenson.weatherwatcher.alerts

import androidx.lifecycle.ViewModel
import com.bswenson.weatherwatcher.data.AlertsRepository
import com.bswenson.weatherwatcher.model.Alert
import java.util.*

class AlertsViewModel : ViewModel() {
    private val alertsRepository = AlertsRepository.get()
    val alertsListLiveData = alertsRepository.getAlerts()

    fun addAlert(alert: Alert){
        alertsRepository.addAlert(alert)
    }

    fun deleteAlert(alert: Alert){
        alertsRepository.deleteAlert(alert)
    }
}
