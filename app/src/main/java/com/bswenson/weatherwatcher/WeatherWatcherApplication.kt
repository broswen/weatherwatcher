package com.bswenson.weatherwatcher

import android.app.Application
import com.bswenson.weatherwatcher.data.AlertsRepository

class WeatherWatcherApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        AlertsRepository.initialize(this)
    }
}