package com.bswenson.weatherwatcher.daily

import android.icu.text.SimpleDateFormat
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.bswenson.weatherwatcher.R
import com.bswenson.weatherwatcher.model.HourlyWeatherResponse
import com.bswenson.weatherwatcher.weekly.WeeklyFragment
import com.bswenson.weatherwatcher.weekly.WeeklyViewModel
import kotlinx.android.synthetic.main.daily_fragment.*
import java.util.*

class DailyFragment : Fragment() {

    companion object {
        fun newInstance() = DailyFragment()
    }

    private lateinit var viewModel: WeeklyViewModel

    private lateinit var recyclerView: RecyclerView

    private lateinit var dateTextView: TextView
    private lateinit var weatherTextView: TextView
    private lateinit var tempTextView: TextView
    private lateinit var feelsLikeTextView: TextView
    private lateinit var humidityTextView: TextView
    private lateinit var coordsTextView: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.daily_fragment, container, false)
        dateTextView = view.findViewById(R.id.dateTextView)
        weatherTextView = view.findViewById(R.id.weatherTextView)
        tempTextView = view.findViewById(R.id.tempTextView)
        feelsLikeTextView = view.findViewById(R.id.feelsLikeTextView)
        humidityTextView = view.findViewById(R.id.humidityTextView)
        coordsTextView = view.findViewById(R.id.coordsTextView)

        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(requireActivity()).get(WeeklyViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.openWeatherResponse.observe(viewLifecycleOwner, Observer {
            dateTextView.text = "${SimpleDateFormat("EEE, MMM d, YYYY").format(Date(it.current.dt * 1000))}"
            weatherTextView.text = it.current.weather.first().main?: "NULL"
            tempTextView.text = "${it.current.temp} F"
            feelsLikeTextView.text = "Feels like ${it.current.feels_like} F"
            humidityTextView.text = "${it.current.humidity}% Humidity"

            iconImageView.setImageDrawable(getDrawable(requireContext(), getWeatherIcon(it.current.weather.first().main)))

            recyclerView.adapter = HourlyWeatherAdapter(it.hourly.take(24))

            coordsTextView.text = "${viewModel.latLiveData.value}, ${viewModel.lonLiveData.value}"
        })

    }

    private fun getWeatherIcon(main: String): Int = when(main){
        "Clouds" -> R.drawable.ic_wi_day_sunny_overcast
        "Clear" -> R.drawable.ic_wi_day_sunny
        "Snow" -> R.drawable.ic_wi_snow
        "Rain" -> R.drawable.ic_wi_rain
        "Drizzle" -> R.drawable.ic_wi_sprinkle
        "Thunderstorm" -> R.drawable.ic_wi_thunderstorm
        else -> R.drawable.ic_wi_fog
    }

    private inner class HourlyItemHolder(val linearLayout: LinearLayout) : RecyclerView.ViewHolder(linearLayout){

        private lateinit var timeTextView: TextView
        private lateinit var tempTextView: TextView
        private lateinit var humidityTextView: TextView

        fun bind(hourlyWeatherResponse: HourlyWeatherResponse){
            timeTextView = linearLayout.findViewById(R.id.timeTextView)
            tempTextView = linearLayout.findViewById(R.id.tempTextView)
            humidityTextView = linearLayout.findViewById(R.id.humidityTextView)

            timeTextView.text = SimpleDateFormat("HH:mm").format(Date(hourlyWeatherResponse.dt * 1000))
            tempTextView.text = "${hourlyWeatherResponse.temp} F"
            humidityTextView.text = "${hourlyWeatherResponse.humidity}% Humidity"
        }
    }

    private inner class HourlyWeatherAdapter(val data: List<HourlyWeatherResponse>) : RecyclerView.Adapter<HourlyItemHolder>(){

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HourlyItemHolder {
            val view = layoutInflater.inflate(R.layout.daily_list_item, parent, false) as LinearLayout

            return HourlyItemHolder(view)
        }

        override fun getItemCount() = data.size

        override fun onBindViewHolder(holder: HourlyItemHolder, position: Int) {
            holder.bind(data[position])
        }
    }

}
